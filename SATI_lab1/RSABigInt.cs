﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace SATI_lab1
{
    /// <summary>
    /// Same as RSA, but can Encrypt and Decrypt really HUGE numbers!
    /// </summary>
    class RSABigInt
    {
        BigInteger D;
        BigInteger E;
        BigInteger N;


        //Already implemented in RSA class
        //public RSA()
        //{
        //    //gen
        //}

        public RSABigInt(BigInteger p, BigInteger q, BigInteger e)
        {
            N = p * q;
            E = e;
            BigInteger nPhi = (q - 1) * (p - 1);
            BigInteger mPhi = NumberTheory.Phi(nPhi);
            D = BigInteger.ModPow(e, mPhi - 1, nPhi);
            //Console.WriteLine("E {0}\nD {1}\nN {2}\np {3}\nq {4}\nnPhi {5}\nmPhi {6}", E, D, N, p, q, nPhi, mPhi);
        }

        public BigInteger Encrypt(BigInteger m)
        {
            return BigInteger.ModPow(m, E, N); //bigInt stores data in own format, so not easy to reimplement this
        }

        public BigInteger Decrypt(BigInteger c)
        {
            return BigInteger.ModPow(c, D, N);
        }

        public static BigInteger RSAEncrypt(BigInteger m, BigInteger e, BigInteger n)
        {
            return BigInteger.ModPow(m, e, n);
        }

        public static BigInteger RSADecrypt(BigInteger c, BigInteger d, BigInteger n)
        {
            return BigInteger.ModPow(c, d, n);
        }
    }
}
