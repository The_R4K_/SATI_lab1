﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Threading.Tasks;

namespace SATI_lab1
{
    class Program
    {

        static void Main(string[] args)
        {
            //variant 8: 4931	600957515	1673827457
            ulong[] pq = NumberTheory.FastGetPrimeMultipiers(1673827457);
            ulong message = 4931;
            Console.WriteLine("p and q is: ");
            Utils.printArray(pq);
            RSA rsa = new RSA(pq[0], pq[1], 600957515);
            {
                Console.WriteLine("m is: "+message);

                Stopwatch s = new Stopwatch();
                s.Start();

                ulong c = rsa.Encrypt(message);
                ulong m = rsa.Decrypt(c);

                s.Stop();

                Console.WriteLine("c is: " + c);
                Console.WriteLine("decrypted m is: " + m);
                Console.WriteLine("used {0}ticks", s.ElapsedTicks);
            }

            Console.WriteLine("\n---  Start CRT TEST! --- \n");

            {
                Stopwatch s = new Stopwatch();
                s.Start();

                ulong c = rsa.Encrypt(message);
                ulong m = rsa.Decrypt(c);

                s.Stop();

                Console.WriteLine("c is: " + c);
                Console.WriteLine("decrypted m is: " + m);
                Console.WriteLine("used {0}ticks", s.ElapsedTicks);
            }


            Console.ReadKey();
        }
    }
}
