﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SATI_lab1
{
    static class Utils
    {
        public static void printArray(int[] arr)
        {
            for (int i = 0; i < arr.Length - 1; i++)
                Console.Write(arr[i] + ", ");
            Console.WriteLine(arr[arr.Length - 1]);
        }
        public static void printArray(ulong[] arr)
        {
            for (int i = 0; i < arr.Length - 1; i++)
                Console.Write(arr[i] + ", ");
            Console.WriteLine(arr[arr.Length - 1]);
        }

        public static void printArray(byte[] arr)
        {
            for (int i = 0; i < arr.Length - 1; i++)
                Console.Write(arr[i] + ", ");
            Console.WriteLine(arr[arr.Length - 1]);
        }
    }
}
