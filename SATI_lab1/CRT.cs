﻿using System;

namespace SATI_lab1
{
    class CRT
    {
        ulong m;
        ulong[] mArr;

        /// <summary>
        /// Contruct the CRT class instance
        /// </summary>
        /// <param name="modulus">modulus on itch CRT repreentations will be based</param>
        public CRT(ulong modulus)
        {
            m = modulus;
            mArr = NumberTheory.getPrimeMultipiers(m);
        }

        /// <summary>
        /// Contruct the CRT class instance
        /// </summary>
        /// <param name="modArr">modulus on itch CRT repreentations will be based</param>
        public CRT(ulong[] modArr)
        {
            m = 1;
            foreach (ulong x in modArr)
                m *= x;
            mArr = modArr;
        }

        /// <summary>
        /// Convert x to array on number in modulus ring
        /// </summary>
        /// <param name="x">number</param>
        /// <returns>CRT representation of x</returns>
        public ulong[] getCRT(ulong x)
        {
            ulong[] arr = new ulong[mArr.Length];
            for (int i = 0; i < mArr.Length; i++)
                arr[i] = x % mArr[i];
            return arr;
        }

        /// <summary>
        /// Retore x from CRT representation
        /// </summary>
        /// <param name="bArr">CRT that represent x in modulus ring</param>
        /// <returns>Restored x</returns>
        public ulong revertCRT(ulong[] bArr)
        {
            ulong x = 0;
            ulong M, MI;
            for (int i = 0; i < mArr.Length; i++)
            {
                M = m / mArr[i];
                MI = NumberTheory.ModPower(M % mArr[i], mArr[i] - 2, mArr[i]);
                Console.WriteLine(MI + " = 1 * " + M + " ^ " + (mArr[i] - 2) + " (mod " + mArr[i] + ")");
                x += M * MI * bArr[i];
            }
            return x % m;
        }
    }
}
