﻿using System;

namespace SATI_lab1
{
    /// <summary>
    /// Class implement RSA algorythm using ulong(0..18,446,744,073,709,551,616),
    /// it's can couse problems with big numbers! But this is just Proof of Concept.
    /// </summary>
    class RSA
    {
        ulong p, q;
        ulong D;
        ulong E;
        ulong N;
        ulong PHI;

        /// <summary>
        /// Create RSA class intance and generate new public and private keys
        /// </summary>
        /// <param name="keyPower">define how many bits will be used to generate p and q.
        /// Accept values from 0(1bit) to 31(32 bit)</param>
        public RSA(int keyPower)
        {
            keyPower = (uint)keyPower > 31 ? 31 : keyPower;
            int min = 1 << keyPower;
            int max = 1 << keyPower + 1;

            Random rnd = new Random();

            ulong p = (ulong)rnd.Next(min, max);
            ulong q = (ulong)rnd.Next(min, max);
            p = NumberTheory.FindNearPrime(p);
            q = NumberTheory.FindNearPrime(q);
            PHI = (p - 1) * (q - 1);
            max = PHI >= int.MaxValue ? int.MaxValue - 1 : (int)PHI;

            ulong e = (ulong)rnd.Next(2, max);

            while(NumberTheory.GCD(e,PHI)!=1)
                e = (ulong)rnd.Next(2, max);

            N = p * q;
            E = e;
            calcKeys(p, q, e);
            this.p = p;
            this.q = q;
        }

        public RSA(ulong p, ulong q, ulong e)
        {
            N = p * q;
            E = e;
            PHI = (p - 1) * (q - 1);
            calcKeys(p, q, e);
            this.p = p;
            this.q = q;
        }
        
        /// <summary>
        /// Calculate private key
        /// </summary>
        /// <param name="p"></param>
        /// <param name="q"></param>
        /// <param name="e"></param>
        private void calcKeys(ulong p, ulong q, ulong e)
        {
            ulong mPhi = NumberTheory.Phi(PHI);
            D = NumberTheory.ModPower(e, mPhi - 1, PHI);
        }

        public ulong Encrypt(ulong m)
        {
            return NumberTheory.ModPower(m, E, N);
        }

        public ulong Decrypt(ulong c)
        {
            return NumberTheory.ModPower(c, D, N);
        }

        public static ulong RSAEncrypt(ulong m, ulong e, ulong n)
        {
           return NumberTheory.ModPower(m, e, n);
        }

        public static ulong RSADecrypt(ulong c, ulong d, ulong n)
        {
            return NumberTheory.ModPower(c, d, n);
        }

        public ulong RSAEncryptFast(ulong m) 
        {
            CRT crt = new CRT(new ulong[] { p, q });
            ulong[] mInCRT = crt.getCRT(m);
            mInCRT = NumberTheory.CRTModPower(mInCRT, E, N);
            return crt.revertCRT(mInCRT);
        }

        public ulong RSADecryptFast(ulong c)
        {
            CRT crt = new CRT(new ulong[] { p, q });
            ulong[] cInCRT = crt.getCRT(c);
            cInCRT = NumberTheory.CRTModPower(cInCRT, E, N);
            return crt.revertCRT(cInCRT);
        }
    }
}
